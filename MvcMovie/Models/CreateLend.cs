using System;

namespace MvcMovie.Models
{
    public class CreateLend
    {
        public int Id { get; set; }

        public string DateTo { get; set; }

        public string DateFrom { get; set; }

    }
}
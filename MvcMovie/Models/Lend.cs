﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MvcMovie.Models
{
    public class Lend
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int LID { get; set; }

        [Display(Name = "ID")]
        public int MovieID { get; set; }

        [Display(Name = "Owner")]
        public string OwnerID { get; set; }

        [Display(Name = "Date From")]
        [DataType(DataType.Date)]
        public Nullable<DateTime> DateFrom { get; set; }

        [Display(Name = "Date To")]
        [DataType(DataType.Date)]
        public Nullable<DateTime> DateTo { get; set; }

        public Movie Movie { get; set; }

        public Status Status { get; set; }
    }

    public enum Status
    {
        Unknown,
        Returned,
        Damaged
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MvcMovie.Models;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.IO;
using System.Text;
using System.Runtime.Serialization.Json;

namespace MvcMovie.Controllers
{
    public class LendsController : Controller
    {
        private readonly MvcMovieContext _context;

        public LendsController(MvcMovieContext context)
        {
            _context = context;
        }

        // GET: Lends
        public async Task<IActionResult> Index()
        {
            var mvcMovieContext = _context.Lends.Include(l => l.Movie);
            return View(await mvcMovieContext.ToListAsync());
        }

        // GET: Lends/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var lend = await _context.Lends
                .Include(l => l.Movie)
                .FirstOrDefaultAsync(m => m.LID == id);
            if (lend == null)
            {
                return NotFound();
            }

            return View(lend);
        }
 
        [HttpPost]
        public async Task<IActionResult> Returned(int id)
        {
            var lend = await _context.Lends
                .Include(l => l.Movie)
                .FirstOrDefaultAsync(m => m.LID == id);


             lend.Status = Status.Returned;

             await _context.SaveChangesAsync();


             return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public async Task<IActionResult> Damaged(int id)
        {
            var lend = await _context.Lends
                .Include(l => l.Movie)
                .FirstOrDefaultAsync(m => m.LID == id);

            
            lend.Status = Status.Damaged;

            await _context.SaveChangesAsync();


            return RedirectToAction(nameof(Index));
        }    
            
         // GET: Lends/Create
        public IActionResult Lend(int id)
        {

            return PartialView("_Lend", new Lend { MovieID = id });
        }

        [HttpPost]
        public IActionResult LendPost([FromBody]CreateLend body)
        {
            var lend = new Lend();
            var userId = User.FindFirst(ClaimTypes.Name).Value;
            lend.OwnerID = userId;

            lend.MovieID = body.Id;

            DateTime DateToLocal = DateTime.Parse(body.DateTo);
            lend.DateTo = DateToLocal;
            DateTime DateFromLocal = DateTime.Parse(body.DateFrom);
            lend.DateFrom = DateFromLocal;

            _context.Add(lend);
            _context.SaveChanges();

            return RedirectToAction(nameof(Index));
        }

        // GET: Lends/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var lend = await _context.Lends
                .Include(l => l.Movie)
                .FirstOrDefaultAsync(m => m.LID == id);
            if (lend == null)
            {
                return NotFound();
            }

            return View(lend);
        }

        // POST: Lends/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var lend = await _context.Lends.FindAsync(id);
            _context.Lends.Remove(lend);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool LendExists(int id)
        {
            return _context.Lends.Any(e => e.LID == id);
        }
    }
}
